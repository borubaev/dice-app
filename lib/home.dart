import 'package:flutter/material.dart';
import 'dart:math';

class DiceApp extends StatefulWidget {
  const DiceApp({super.key, required this.title});

  final String title;

  @override
  State<DiceApp> createState() => DiceGame();
}

class DiceGame extends State<DiceApp> {
  int leftdice = 1;
  int rightdice = 2;

  void ishtoo() {
    setState(() {
      leftdice = Random().nextInt(6) + 1;
      rightdice = Random().nextInt(6) + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dice App in flutter'),
      ),
      body: Center(
        child: Row(
          children: [
            Expanded(
              child: FloatingActionButton.large(
                backgroundColor: Colors.white,
                onPressed: () {
                  ishtoo();
                },
                child: Image.asset(
                  'assets/images/$leftdice.png',
                  width: double.infinity,
                  height: 450,
                ),
              ),
            ),
            const SizedBox(
              width: 30,
            ),
            Expanded(
              child: FloatingActionButton.large(
                backgroundColor: Colors.white,
                onPressed: () {
                  ishtoo();
                },
                child: Image.asset(
                  'assets/images/$rightdice.png',
                  width: double.infinity,
                  height: 450,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
